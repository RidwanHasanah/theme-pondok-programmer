 <?php 
  wp_footer();
  require_once('components/front-page/footer.inc.php');
 ?>
<!-- Copyright -->

<div class="footer-copyright elegant-color-dark text-center py-3">
    <p class="wow fadeInUp slow m-0 grey-text">© 2019 Copyright&nbsp;Pondok Programmer | <a class="text-white" href="http://pondokit.com">PondokIT.com</a></p>
  </div>
  <!-- /Copyright -->
  <script type="text/javascript">
  new WOW().init();
  </script>
</body>

</html>