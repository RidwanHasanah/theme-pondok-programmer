<?php
function faq_post_type() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        // 'excerpt', // post excerpt
        // 'custom-fields', // custom fields
        // 'comments', // post comments
        // 'revisions', // post revisions
        // 'post-formats', // post formats
        // 'trackbacks',
        // 'page-attributes'
        );
    $labels = array(
        'name' => _x('faqs', 'plural'),
        'singular_name' => _x('faq', 'singular'),
        'menu_name' => _x('FAQ', 'admin menu'),
        'name_admin_bar' => _x('faq', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New FAQ'),
        'new_item' => __('New FAQ'),
        'edit_item' => __('Edit FAQ'),
        'view_item' => __('View FAQ'),
        'all_items' => __('All FAQ'),
        'search_items' => __('Search faq'),
        'not_found' => __('No faq found.'),
        );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news'),
        'has_archive' => true,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-book',
        'menu_position' => 4
        );

    register_post_type( 'faq',$args);
  }
  add_action( 'init', 'faq_post_type' );
?>