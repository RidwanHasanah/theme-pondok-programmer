<?php
function team_post_type() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        // 'excerpt', // post excerpt
        // 'custom-fields', // custom fields
        // 'comments', // post comments
        // 'revisions', // post revisions
        // 'post-formats', // post formats
        // 'trackbacks',
        // 'page-attributes'
        );
    $labels = array(
        'name' => _x('teams', 'plural'),
        'singular_name' => _x('team', 'singular'),
        'menu_name' => _x('Our Team', 'admin menu'),
        'name_admin_bar' => _x('team', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New Member'),
        'new_item' => __('New Member'),
        'edit_item' => __('Edit Member'),
        'view_item' => __('View Member'),
        'all_items' => __('All Member'),
        'search_items' => __('Search member'),
        'not_found' => __('No member found.'),
        );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news'),
        'has_archive' => true,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-id-alt',
        'menu_position' => 2
        );

    register_post_type( 'team',$args);
  }
  add_action( 'init', 'team_post_type' );
?>