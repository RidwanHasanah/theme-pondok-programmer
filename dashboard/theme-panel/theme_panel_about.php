<?php
function theme_panel_display_about(){
    if ($_POST['about-submit']) {
        $options['about-title1'] = $_POST['about-title1']; //initial value
        $options['about-title2'] = $_POST['about-title2'];
        $options['about-desc1'] = $_POST['about-desc1'];
        $options['about-desc2'] = $_POST['about-desc2'];

        update_option('about-fields',$options);

        echo '<div class="updated"><p><b>Option Saved </b></p></div>';
    }
    $options = get_option('about-fields');
    ?>
    <div class="" style="border: px solid #2BBBAD; border-radius: 5px; background: #fff; padding: 20px;" >
    <h1>Theme Panel</h1>
   <br>
   <h2>About</h2>
   <hr>
   <form class="form" action="" method="post">
       <?php settings_fields( 'theme-panel' ); ?>
       <?php do_settings_sections( 'theme-panel' ); ?>
       <table class="form-table">
           <tr>
               <td><label for=""><b>Title 1</b></label></td>
               <td><input type="text" name="about-title1" id="about-title1" value="<?php echo $options['about-title1']; ?>"></td>
           </tr>
           <tr>
               <td><label for=""><b>Description 1</b></label></td>
               <td><textarea id="about-desc1" type="text" name="about-desc1"><?php echo $options['about-desc1']; ?></textarea></td>
           </tr>
           
           <tr>
               <td><label for=""><b>Title 2</b></label></td>
               <td><input id="about-title2" value="<?php echo $options['about-title2']; ?>" type="text" name="about-title2"></td>
           </tr>
           <tr>
               <td><label for=""><b>Desc 2</b></label></td>
               <td><textarea id="about-desc2" type="text" name="about-desc2"><?php echo $options['about-desc2']; ?></textarea></td>
           </tr>

       </table>
        <input type="submit" id="about-submit" name="about-submit" value="Save Changes" class="button-primary">
   </form> 
   </div>
    <?php
}
?>