<?php
function theme_panel_display_focus(){
    if ($_POST['focus-submit']) {
        $options['focus-title'] = $_POST['focus-title']; //initial value
        $options['focus-subtitle'] = $_POST['focus-subtitle'];
        $options['focus-contitle1'] = $_POST['focus-contitle1'];
        $options['focus-contitle2'] = $_POST['focus-contitle2'];
        $options['focus-contitle3'] = $_POST['focus-contitle3'];
        $options['focus-desctitle1'] = $_POST['focus-desctitle1'];
        $options['focus-desctitle2'] = $_POST['focus-desctitle2'];
        $options['focus-desctitle3'] = $_POST['focus-desctitle3'];

        $options['focus-icon1'] = $_POST['focus-icon1'];
        $options['focus-icon2'] = $_POST['focus-icon2'];
        $options['focus-icon3'] = $_POST['focus-icon3'];

        update_option('focus-fields',$options);

        echo '<div class="updated"><p><b>Option Saved </b></p></div>';
    }
    $options = get_option('focus-fields');
    ?>
    <div class="" style="border: px solid #2BBBAD; border-radius: 5px; background: #fff; padding: 20px;" >
    <h1>Theme Panel</h1>
   <br>
   <h2>Fokus</h2>
   <hr>
   <form class="form" action="" method="post">
       <?php settings_fields( 'theme-panel' ); ?>
       <?php do_settings_sections( 'theme-panel' ); ?>
       <table class="form-table">
           <tr>
               <td><label for=""><b>Title</b></label></td>
               <td><input type="text" name="focus-title" id="focus-title" value="<?php echo $options['focus-title']; ?>"></td>
           </tr>
           <tr>
               <td><label for=""><b>Sub Title</b></label></td>
               <td><input id="focus-subtitle" value="<?php echo $options['focus-subtitle']; ?>" type="text" name="focus-subtitle"></td>
           </tr>
           
           <tr>
               <td><label for=""><b>Content Title 1</b></label></td>
               <td><input id="focus-contitle1" value="<?php echo $options['focus-contitle1']; ?>" type="text" name="focus-contitle1"></td>
           </tr>
           <tr>
               <td><label for=""><b>Content Desc 1</b></label></td>
               <td><textarea id="focus-desctitle1" type="text" name="focus-desctitle1"><?php echo $options['focus-desctitle1']; ?></textarea></td>
           </tr>
           <tr>
               <td><label for=""><b>Icon Content 1</b></label></td>
               <td><input id="focus-icon1" value="<?php echo $options['focus-icon1']; ?>" type="text" name="focus-icon1"></td>
           </tr>


           <tr>
               <td><label for=""><b>Content Title 2</b></label></td>
               <td><input id="focus-contitle2" value="<?php echo $options['focus-contitle2']; ?>" type="text" name="focus-contitle2"></td>
           </tr>
           <tr>
               <td><label for=""><b>Content Desc 2</b></label></td>
               <td><textarea id="focus-desctitle2" type="text" name="focus-desctitle2"><?php echo $options['focus-desctitle2']; ?></textarea></td>
           </tr>
           <tr>
               <td><label for=""><b>Icon Content 2</b></label></td>
               <td><input id="focus-icon2" value="<?php echo $options['focus-icon2']; ?>" type="text" name="focus-icon2"></td>
           </tr>


           <tr>
               <td><label for=""><b>Content Title 3</b></label></td>
               <td><input id="focus-contitle3" value="<?php echo $options['focus-contitle3']; ?>" type="text" name="focus-contitle3"></td>
           </tr>
           <tr>
               <td><label for=""><b>Content Desc 3</b></label></td>
               <td><textarea id="focus-desctitle3" type="text" name="focus-desctitle3"><?php echo $options['focus-desctitle3']; ?></textarea></td>
           </tr>
           <tr>
               <td><label for=""><b>Icon Content 3</b></label></td>
               <td><input id="focus-icon3" value="<?php echo $options['focus-icon3']; ?>" type="text" name="focus-icon3"></td>
           </tr>

       </table>
        <input type="submit" id="focus-submit" name="focus-submit" value="Save Changes" class="button-primary">
   </form> 
   <p>Reference icon <a href="https://fontawesome.com/icons?d=gallery" target="_blank">ICON</a></p>
   </div>
    <?php
}
?>