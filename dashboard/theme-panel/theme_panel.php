<?php
function theme_panel_menu(){
    add_menu_page(
        'Theme Option Title',
        'Theme Panel', //name Menu
        'manage_options', //required 'manage_options || capability to get there though (admins only)
        'theme-options', //required menu slug 'theme-options
        'theme_panel_display_focus', //function for display
        'dashicons-admin-generic', //icon
        2 //position
    );
    add_submenu_page('theme-options', 'About', 'Panel About', 'manage_options', 'theme-panel-about', 'theme_panel_display_about');
}

add_action('admin_menu','theme_panel_menu');

// Added Display Option On Dashboard
require_once('theme_panel_focus.php');
require_once('theme_panel_about.php');