<?php
function testimonial_post_type() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
        'trackbacks',
        'page-attributes'
        );
    $labels = array(
        'name' => _x('testimonials', 'plural'),
        'singular_name' => _x('testimonial', 'singular'),
        'menu_name' => _x('Testimonial', 'admin menu'),
        'name_admin_bar' => _x('testimonial', 'admin bar'),
        'add_new' => _x('Add New', 'add new'),
        'add_new_item' => __('Add New testimonial'),
        'new_item' => __('New testimonial'),
        'edit_item' => __('Edit testimonial'),
        'view_item' => __('View testimonial'),
        'all_items' => __('All testimonial'),
        'search_items' => __('Search testimonial'),
        'not_found' => __('No testimonial found.'),
        );
    $args = array(
        'supports' => $supports,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'news'),
        'has_archive' => true,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-groups',
        'menu_position' => 3
        );

    register_post_type( 'testimonial',$args);
  }
  add_action( 'init', 'testimonial_post_type' );
?>