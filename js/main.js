$(document).ready(function(){

  
  $( "#click" ).scroll(function() {
    $( "#fokus" ).slideDown( "slow", function() {
      // Animation complete.
    });
  });
    
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){
     
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });

  let circleProgress = new progressBar({
    type: "circle", //top, circle
    targetClass: "round-progress",
    textClass: "text",
    value: 100, //final value
    duration: 2000, //ms
    completeDuration: 500 //ms
});
setTimeout(() => {
    circleProgress.complete();
}, 2000);

let circleProgress2 = new progressBar({
    type: "circle", //top, circle
    targetClass: "round-progress2",
    textClass: "text2",
    value: 50, //final value
    duration: 4000, //ms
    completeDuration: 500 //ms
});
setTimeout(() => {
    circleProgress2.complete();
}, 2000);

let circleProgress3 = new progressBar({
    type: "circle", //top, circle
    targetClass: "round-progress3",
    textClass: "text3",
    value: 50, //final value
    duration: 4000, //ms
    completeDuration: 500 //ms
});
setTimeout(() => {
    circleProgress3.complete();
}, 2000);