<?php
require_once('inc/pagination.inc.php');
require_once('inc/template-tags.inc.php');
/**
 * Include CSS files
 */
function theme_enqueue_scripts() {
    wp_enqueue_style( 'Font_Awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'MDB', get_template_directory_uri() . '/css/mdb.min.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );

    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js');
    wp_enqueue_script( 'Tween', get_template_directory_uri() . '/js/modules/tween.js');
    wp_enqueue_script( 'progressBar', get_template_directory_uri() . '/js/modules/progressBar.js');
    wp_enqueue_script( 'Navbar', get_template_directory_uri() . '/js/modules/scrolling-navbar.js');
    wp_enqueue_script( 'Wow', get_template_directory_uri() . '/js/modules/wow.js');
    
    wp_enqueue_script( 'Tether', get_template_directory_uri() . '/js/popper.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'Bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'MDB', get_template_directory_uri() . '/js/mdb.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'Main', get_template_directory_uri() . '/js/main.js');

    }
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function setup_init(){
    register_nav_menus(array(
        'main_menu' => 'Menu Utama'
    ));

    // add feadtured image
    add_theme_support('post-thumbnails');
    add_image_size('medium-large', '300', '300', true);
    add_image_size('single_post');
    add_image_size('rounded', '140', '140', true);
    add_image_size('medium', '250', '250', true);
    add_image_size('program', '366', '625', true);
    // Custome Logo
    add_theme_support( 'custom-logo', array(
        'height'      => 50,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );
}

add_action('after_setup_theme', 'setup_init');

function theme_prefix_the_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}

function widget_setup(){
    register_sidebar(array(
        'name' => 'Sidebar Pertama',
        'id' => 'sidebar1',
        // 'class'         => 'sidebar',
        'before_widget' => '<li id="%1$s" class="sidebar %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));
    register_sidebar(array(
        'name' => 'Sidebar Kedua',
        'id' => 'sidebar2',
        'description'   => '',
        'class'         => 'sidebar',
        'before_widget' => '<li id="%1$s" class="sidebar %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));
}

add_action('widgets_init', 'widget_setup');

require_once('dashboard/theme-panel/setup_init.php');

?>