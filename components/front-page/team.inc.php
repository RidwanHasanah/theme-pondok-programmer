<!-- Team-->
<?php
function team_display()
{
  ?>
  <div class="col-md-4 mb-4 slideInUp slower wow text-white my-5" data-wow-delay="0.3s" "="" style=" visibility: visible; animation-name: slideInUp; animation-delay: 0.3s;">
    <div class="wow fadeInLeft slow card shadow relative p-5 my-5" data-wow-delay="0.3s" style="background: #4BCBB2;">
      <?php the_post_thumbnail('rounded', array('class' => 'hero-profile rounded-circle shadow')); ?>
      <h3 class="raleway-regular mt-5">
        <a class="text-white" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      </h3>
      <label class="lead mt-3"><?php the_excerpt(); ?></label>
    </div>
  </div>
<?php
}
?>
<div class="ceo">
  <div class="container py-5 text-center">
    <div class=py-5">
      <h1 class="wow fadeIn slow raleway-medium" data-wow-delay="0.9s">Team Pondok Programmer</h1>
      <hr class="wow zoomIn hero-title-line">

      <div class="row justify-content-center">
        <?php
        query_posts(array('post_type' => 'team'));
        $count_posts = wp_count_posts('testimonial');
        $published_posts = $count_posts->publish;
        $i = 0;
        if ($published_posts <= 3) {
          while (have_posts()) {
            the_post();
            team_display();
          }
        } else {
          do {
            the_post();
            team_display();
            $i++;
          } while ($i <= 2);
        }
        ?>
      </div>
    </div>
  </div>
</div>
<!-- /Team-->