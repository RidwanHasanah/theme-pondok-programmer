<!-- Footer -->
<footer class="contact elegant-color font-small" id="contact">

<div class="default-color">
  <div class="container">

    <!-- Grid row-->
    <div class="row py-4 d-flex align-items-center">

      <!-- Grid column -->
      <div class="col-md-6 col-lg-5 text-center text-white text-md-left mb-4 mb-md-0">
        <h6 class="mb-0">Terhubung dengan kami di Sosial Media!</h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-6 col-lg-7 text-center text-md-right">

        <!-- Facebook -->
        <a class="fb-ic" href="https://www.facebook.com/baznas.bone.5" target="_blank">
          <i class="fa fa-facebook white-text mr-4"> </i>
        </a>
        <!-- Twitter -->
        <!-- <a class="tw-ic">
          <i class="fa fa-twitter white-text mr-4"> </i>
        </a> -->
        <!-- Google +-->
        <!-- <a class="gplus-ic">
          <i class="fa fa-google-plus white-text mr-4"> </i>
        </a> -->
        <!--Linkedin -->
        <!-- <a class="li-ic">
          <i class="fa fa-linkedin white-text mr-4"> </i>
        </a> -->
        <!--Instagram-->
        <a href="https://www.instagram.com/baznaskab.bone/" target="_blank" class="ins-ic">
          <i class="fa fa-instagram white-text"> </i>
        </a>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
</div>

<div class="container py-5 text-white">
  <div class="row">
    <div class="col-md-6 col-sm-12 text-md-left text-center mb-4">
      <h3 class="wow fadeIn slow raleway-medium" data-wow-delay="0.9s">Contact</h3>
      <hr class="wow zoomIn hero-title-line accent-2 mb-4 mt-0 d-inline-block mx-auto">
        <p class="wow fadeInUp raleway-regular"><i class="fa fa-map-marker mr-3" aria-hidden="true" data-wow-delay="0.9s"></i>jl. Jend Ahmad Yani , Mesjid Agung Al Ma'arif Lt. 1</p>
        <p class="wow fadeInUp raleway-regular"><i class="fa fa-phone mr-3" aria-hidden="true" data-wow-delay="1.2s"></i>(6281)58044079</p>
        <p class="wow fadeInUp raleway-regular"><i class="fa fa-envelope mr-3" aria-hidden="true" data-wow-delay="1.5s"></i>baznaskab.bone@baznas.go.id</p>
    </div>
    <div class="col-md-6 col-sm-12 text-md-left text-center mb-4">
      <h3 class="wow fadeIn slow raleway-medium" data-wow-delay="1.2s">Tentang</h3>
      <hr class="wow zoomIn hero-title-line accent-2 mb-4 mt-0 d-inline-block mx-auto">
      <p class="wow fadeInUp slow raleway-regular" data-wow-delay="1.5s">Badan Amil Zakat Nasional (BAZNAS) kab. Bone merupakan lembaga resmi yang dibentuk oleh pemerintah dan pengurusnya yang dilantik oleh Bupati Bone berdasarkan keputusan yang memiliki tugas dan fungsi menghimpun dan menyalurkan zakat, infaq, sedekah (ZIS).</p>
    </div>
  </div>
</div>
</footer>
<!-- /Footer -->