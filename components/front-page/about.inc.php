  <!-- Pondok Programmer -->
<?php $options = get_option('about-fields'); ?>

  <div class="pondok-programmer text-center text-white" id="tentang">
    <div class="container py-5">
      <div class="py-5">
        <h1 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.3s"><?php echo $options['about-title1']; ?></h1>
        <hr class="wow zoomIn hero-title-line-light">
        <p class="wow fadeIn slow lead raleway-regular mt-4 mb-5" data-wow-delay="0.9s"><?php echo $options['about-desc1']; ?></p>
      </div>
    </div>
    <img src="<?php echo get_template_directory_uri() . '/img/svg/hiyut-bottom.svg' ;?>" alt="" class="white-hiyut">
  </div>
  <!-- /Pondok Programmer-->

  <!-- Tentang-->
  <div class="tentang text-center">
    <div class="float-card py-5 px-md-5">
      <h1 class="wow fadeIn slow raleway-medium" data-wow-delay="0.9s"><?php echo $options['about-title2']; ?>i</h1>
      <hr class="wow zoomIn hero-title-line">
        <p class="wow fadeIn slow lead raleway-regular mt-4" data-wow-delay="1.2s"><?php echo $options['about-desc2']; ?></p>
    </div>
    
  </div>
  
  <!-- /Tentang-->