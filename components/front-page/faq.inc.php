<div class="faq container">
      <div class="row mb-5 pb-5">
      <?php
        query_posts(array('post_type'=>'faq'));
        if( have_posts())
          {
          while(have_posts())
          {
            the_post();
            ?>
          <div class="col-md-6 col-sm-12">
              <button type="button" class="wow flipInX my-2 btn btn-outline-blue-grey btn-block text-md-left text-sm-center" data-toggle="collapse" data-target="#faq<?php the_ID(); ?>" aria-expanded="false" aria-controls="faq<?php the_ID(); ?>" data-wow-delay="0.3s">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php the_title(); ?>
              </button>
              <div class="collapse text-md-left text-sm-center font-small" id="faq<?php the_ID(); ?>">
                <div class="mt-3">
                  <label class="raleway-regular grey-text mx-2">
                  <?php the_content(); ?>
                  </label>
                </div>
              </div>
          </div>
            <?php
          }   
          }
        ?>
      </div>
  </div>
  <div class="hero-more">
      <div class="row mx-auto container py-5">
        <div class="col-md-6 col-sm-12 text-md-right text-center">
          <h3 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.3s">Prosentase Pembelajaran</h3>
          <div class="pt-1 pb-5">
            <div class="row text-center">
              <div class="wow fadeInUp slow col-md-4 col-sm-12 my-5 my-md-3" data-wow-delay="0.9s">
                <div class="text">0%</div>
                <div class="position">
                  <svg height="100%" width="100%">
                      <circle class="round-progress" cx="50" cy="50" r="45" />
                  </svg>
                </div>
                <label class="raleway-medium mt-5 pt-5">Pondok IT</label>
              </div>
              <div class="wow fadeInUp slow col-md-4 col-sm-12 my-5 my-md-3" data-wow-delay="0.6s">
                <div class="text2">0%</div>
                <div class="position2">
                  <svg height="100%" width="100%">
                      <circle class="round-progress2" cx="50" cy="50" r="45" />
                  </svg>
                </div>
                <label class="raleway-medium mt-5 pt-5">Agama</label>
              </div>
              <div class="wow fadeInUp slow col-md-4 col-sm-12 my-5 my-md-3" data-wow-delay="0.3s">
                <div class="text3">0%</div>
                <div class="position3">
                  <svg height="100%" width="100%">
                      <circle class="round-progress3" cx="50" cy="50" r="45" />
                  </svg>
                </div>
                <label class="raleway-medium mt-5 pt-5">Ilmu IT</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 text-md-left text-center mb-md-0 mb-4">
          <h3 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.6s">Tentang Kakak Asuh Indonesia</h3>
          <label class="wow fadeInUp slow raleway-regular grey-text mt-md-5 mt-3 mr-md-5 mr-sm-0" data-wow-delay="0.9s">
            Program <a href="https://kakakasuhindonesia.org/">Kakak Asuh Indonesia</a> adalah Program dimana orang yang ingin menjadi kakak asuh untuk santri santri Pondok IT.
          </label>
        </div>

        <div class="col-md-6 col-sm-12 text-md-right text-center mt-sm-1 mt-sm-5 mb-md-0 mb-4">
          <h3 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.9s">Tentang Pondok Programmer</h3>
          <label class="wow fadeInUp slow raleway-regular grey-text mt-md-5 mt-3 ml-md-5 ml-sm-0" data-wow-delay="1.2s">
            Pondok Programmer merupakan komunitas IT berbasis Pondok yang senantiasa berusaha mendidik santrinya untuk berakhlaq mulia, professional serta bermanfaat untuk umat.
          </label>
        </div>
        <div class="col-md-6 col-sm-12 text-md-left text-center mt-sm-1 mt-sm-5 mb-md-0 mb-4">
          <h3 class="wow fadeInDown slow raleway-medium" data-wow-delay="1.2s">Visi dan Misi Pondok Programmer</h3>
          <label class="wow fadeInUp slow raleway-regular grey-text mt-md-5 mt-3 mr-md-5" data-wow-delay="1.5s">
            <h4 class="raleway-medium">Visi</h4>
            <p>
              Menjadi Pondok IT Terbaik di Indonesia
            </p>
            <h4 class="raleway-medium">Misi</h4>
            <p>
              Mendidik Santri menjadi manusia yang Bertaqwa, Professional, dan Berbagi
            </p>
          </label>
        </div>
      </div>
  </div>