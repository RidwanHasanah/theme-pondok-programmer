 <!-- Pendaftaran -->
 <div class="pendaftaran" id="daftar">
    <div class="container my-5 text-center">
      <h1 class="wow fadeIn slow raleway-medium" data-wow-delay="0.9s">Pendaftaran</h1>
      <hr class="wow zoomIn hero-title-line">
      <h5 class="wow fadeInUp slow mb-5 pb-5">Pendaftaran telah dibuka, Daftarkan diri anda <a href="http://pendaftaran.pondokit.com" target="_blank">disini</a>.</h5>
    </div>
  </div>
  <!-- /Pendaftaran -->