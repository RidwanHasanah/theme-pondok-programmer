<!-- Produk -->
<div class="produk" id="produk">
    <div class="container py-5 text-center text-white">
      <h1 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.3s">Produk Kami</h1>
      <hr class="wow zoomIn hero-title-line-light">
      <div class="row my-5 py-5">

        <div class="zoom col-md-4 col-sm-12">
          <a href="https://bikinweb.pondokprogrammer.com">
          <img src="<?php echo get_template_directory_uri() . '/img/brands/photographite.svg' ;?>" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="0.6s">
          </a>
          <h5 class="raleway-medium mt-4 mx-5">Photographite Mobile Apps</h5>
        </div>

        <div class="zoom col-md-4 col-sm-12">
            <a href="https://bikinweb.pondokprogrammer.com">
          <img src="https://lh3.googleusercontent.com/-_jg9inyKkvWIFpaUu7MhDtlvmgqmZUxdmGjT_MCnEdR4GcCkhQlPo2J4Ry-2n2ukg=s180-rw" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="1.2s">
          </a>
          <h5 class="raleway-medium mt-4 mx-5">Event Muslim Apps</h5>
        </div>

        <div class="zoom col-md-4 col-sm-12">
            <a href="https://kakakasuhindonesia.org">
          <img src="https://lh3.googleusercontent.com/BLcFxt9YHcvJL21lDxhHn4Sdae5awG5lTmkKa6cQYT9dDTyu9emNqTPDM3hObqGrbYM=s180-rw" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="0.9s">
            </a>
          <h5 class="raleway-medium mt-4 mx-5">Kakak Asuh Indonesia Apps</h5>
        </div>

        <div class="zoom col-md-4 col-sm-12 my-md-5 my-sm-3">
            <a href="https://bikinweb.pondokprogrammer.com">
          <img src="<?php echo get_template_directory_uri() . '/img/brands/sudoro.svg' ;?>" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="0.6s">
            </a>
          <h5 class="raleway-medium mt-4 mx-5">Super Donor</h5>
        </div>

        <!-- <div class="zoom col-md-4 col-sm-12 my-md-5 my-sm-3">
            <a href="https://bikinweb.pondokprogrammer.com">
          <img src="<?php echo get_template_directory_uri() . '' ;?>http://www.pondokprogrammer.com/images/prettyPhotoImages/3.png" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="1.2s">
            </a>
          <h5 class="raleway-medium mt-4 mx-5">Jasa Programmer</h5>
        </div>

        <div class="zoom col-md-4 col-sm-12 my-md-5 my-sm-3">
            <a href="https://bikinweb.pondokprogrammer.com">
          <img src="<?php echo get_template_directory_uri() . '' ;?>http://www.pondokprogrammer.com/images/prettyPhotoImages/4.png" width="250px" height="250px" class="wow zoomIn img-fluid shadow rounded" data-wow-delay="0.9s">
            </a>
          <h5 class="raleway-medium mt-4 mx-5">Lampu Themes</h5>
        </div> -->

      </div>
    </div>
    <img class="white-wave" src="<?php echo get_template_directory_uri() . '/img/svg/wave-bottom-white.svg';?>">
  </div>
  <!-- /Produk -->