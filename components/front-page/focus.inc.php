<!-- Fokus-->
<?php $options = get_option("focus-fields" ); ?>
<div  class="relative py-5" id="fokus">
    <div class="container mx-auto">
      <div class="py-5 text-center">
        <h1 class="wow fadeInDown slow raleway-medium" data-wow-delay="0.3s"><?php echo $options['focus-title']; ?></h1>
        <hr class="wow zoomIn hero-title-line">
        <p class="wow fadeIn slow lead raleway-regular mt-4" data-wow-delay="0.9s"><?php echo $options['focus-subtitle']; ?></p>

        <div class="row my-5 py-5">
          <div class="col-md-4 col-sm-12">
            <i class="wow flipInX slow fa fa-<?php echo $options['focus-icon1']; ?> display-1" data-wow-delay="0.3s" aria-hidden="true"></i>
            <h3 class="wow fadeInDown slow text-default mt-3 raleway-regular" data-wow-delay="0.6s"><?php echo $options['focus-contitle1']; ?></h3>
            <p class="wow fadeInUp slow raleway-regular" data-wow-delay="0.6s"><?php echo $options['focus-desctitle1']; ?></p>
          </div>


          <div class="col-md-4 col-sm-12">
            <i class="wow flipInX slow fa fa-<?php echo $options['focus-icon2']; ?> display-1" data-wow-delay="0.6s" aria-hidden="true"></i>
            <h3 class="wow fadeInDown slow text-default mt-3 raleway-regular" data-wow-delay="0.9s"><?php echo $options['focus-contitle2']; ?></h3>
            <p class="wow fadeInUp slow raleway-regular" data-wow-delay="0.9s"><?php echo $options['focus-desctitle2']; ?></p>
          </div>


          <div class="col-md-4 col-sm-12">
            <i class="wow flipInX slow fa fa-<?php echo $options['focus-icon3']; ?> display-1" data-wow-delay="0.9s" aria-hidden="true"></i>
            <h3 class="wow fadeInDown slow text-default mt-3 raleway-regular" data-wow-delay="1.2s"><?php echo $options['focus-contitle3']; ?></h3>
            <p class="wow fadeInUp slow raleway-regular" data-wow-delay="1.2s"><?php echo $options['focus-desctitle3']; ?></p>
          </div>


        </div>
      </div>
    </div>
    <img class="green-wave" src="<?php echo get_template_directory_uri() . '/img/svg/wave-bottom.svg';?>"/>
  </div>
  <!-- /Fokus-->