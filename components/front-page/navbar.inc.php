<!-- Navbar-->
<?php 
$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>
<nav <?php is_front_page()?'':'style="background-color:#4BCBB2;"' ?> class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <a href="<?php echo home_url(); ?>">
      <img src="<?php echo $image[0];?>" class="wow fadeInLeft slow hero-nav-brand ml-md-5" alt=""/>
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="basicExampleNav">
    <?php
      $args = array(
        'menu'            => '',
        'container'       => 'div',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'item_spacing'    => 'preserve',
        'depth'           => 0,
        'walker'          => '',
        'theme_location'  => 'main_menu',
);
      wp_nav_menu($args); 
    ?>
    </div>
  </nav>
  <!-- /Navbar-->