 <!-- Testimoni -->
 <?php
 function testi_display(){
  ?>
  <div class="col-md-6 col-sm-12 my-md-3 mb-sm-5 py-md-3 pb-sm-5">
    <div class="wow fadeInLeft slow card shadow relative p-5 my-3" data-wow-delay="0.3s">
    <?php the_post_thumbnail( 'rounded', array( 'class'=> 'hero-profile rounded-circle shadow')); ?>
      <h3 class="raleway-regular mt-5">
        <a href="<?php the_permalink();?>"><?php the_title();?></a>
      </h3>
      <label class="lead mt-3"><?php the_excerpt(); ?></label>
    </div>
  </div>
  <?php
 }
 ?>
 <div class="testimoni">
    <div class="container py-5 text-center">
      <div class="">
        <h1 class="wow fadeIn slow raleway-medium" data-wow-delay="0.9s">Testimoni Santri</h1>
        <hr class="wow zoomIn hero-title-line">
        <br>
        <div class="row my-5 py-5">
          <?php
          query_posts(array('post_type'=>'testimonial'));
          $count_posts = wp_count_posts('testimonial');
          $published_posts = $count_posts->publish;
          
          $i = 0;
          if ( $published_posts <= 4) {
            while(have_posts()){ 
              the_post();
              testi_display();
            }
          }else{
            do{
            the_post();
            testi_display();
            $i++;
            }while($i <= 3);
          }
          ?>

        </div>
      </div>
    </div>
  </div>
  <!-- /Testimoni-->