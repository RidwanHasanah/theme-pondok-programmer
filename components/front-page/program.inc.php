<!-- Program -->
<?php
     function display(){
      ?>
      <div class="col-md-3 mb-4 slideInLeft slower wow" data-wow-delay="0.3s" style="visibility: visible; animation-name: slideInLeft; animation-delay: 0.3s;">

      <!--Card-->
      <div class="card default-color-dark ">

        <!--Card image-->
        <div class="view">
          <?php the_post_thumbnail('program', array('class' => 'card-img-top')); ?>
          <a href="<?php the_permalink(); ?>" target="_blank">
            <div class="mask rgba-white-slight waves-effect waves-light"></div>
          </a>
        </div>

        <!--Card content-->
        <div class="card-body text-center white-text">
          <!--Title-->
          <h4 class="card-title"><?php the_title(); ?></h4>
          <!-- <p>
            Memberikan bantuan kepada keluarga miskin yang memiliki keterampilan ekonomi akan tetapi mampu mewujudkan prekonomian keluarga dengan baik diseabkan karena keterbatasan modal kerja.
          </p> -->
          <!--Text-->
          <a href="#" target="_blank" class="btn btn-outline-white btn-md waves-effect">Donasi Sekarang</a>
        </div>

      </div>
      <!--/.Card-->

      </div>
      <?php
    }
    ?>
<div id="program" class="container-fluid py-5">
    <h1 class="text-center card-title h1">Program Berlangsung</h1>
    <!-- Grid row -->
    <div class="row justify-content-center">

    <?php
      wp_reset_query();
      query_posts(array('category_name' => 'program','posts_per_page'=>3));
      if (have_posts()) {
        while (have_posts()) {
          the_post();
          display();
        }
      }

    ?>

    </div>
    <div class="text-center">
      <a id="to-demo" href="<?php echo get_home_url().'/blog'; ?>" class=" btn btn-danger waves-effect waves-light">Semua Program</a>
    </div>
  </div>
  <!-- Program -->