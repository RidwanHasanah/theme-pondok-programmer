<?php 
get_header();
?>

  <!-- Landing-->
  <div class="landing animated fadeIn" id="landing">
    <div class="overlay">
      <div class="hero-landing">
          <img class="animated zoomInDown hero-logo" src="<?php echo get_template_directory_uri() . '/img/brands/bazpusat.png'; ?>"/>
      </div>
      <a href="#fokus">
        <img class="wow fadeInDown hero-arrow-down mx-auto" src="<?php echo get_template_directory_uri() . '/img/svg/arrow_down.svg'; ?>" data-wow-delay="0.6s"/>
        <img id="click" class="wow fadeInDown hero-arrow-down2 mx-auto" src="<?php echo get_template_directory_uri() . '/img/svg/arrow_down.svg'; ?>" data-wow-delay="0.3s"/>
      </a>
    </div>    
  </div>
  <!-- /Landing-->

  <?php 
    require_once('components/front-page/focus.inc.php');
    require_once('components/front-page/about.inc.php');
    require_once('components/front-page/program.inc.php'); //Khusus untuk Baznasbone.org
    // require_once('components/front-page/faq.inc.php');
    // require_once('components/front-page/team.inc.php');
    // require_once('components/front-page/testimoni.inc.php');
    // require_once('components/front-page/product.inc.php');
    // require_once('components/front-page/register.inc.php');
    // require_once('components/front-page/profile.inc.php');
    // require_once('components/front-page/map.inc.php');
    ?>
    
  <?php get_footer();?>
