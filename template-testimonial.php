<?php
/*Template Name: Testimonial*/
 get_header(); 
 
 ?>

<div class="produk p-5">
<div class="container-fluid pt-5 text-white">
<h3>Halaman <?php the_title(); ?></h3>
<div class="row">

<div class="col-md-9">
<main>
  <div class="row p-2">
    <?php
    query_posts(array(
      'post_type' => 'testimonial'
   ));
    if( have_posts())
    {
    while(have_posts())
    {
        the_post();
        ?>
        <div class="col-md-4 mb-4 slideInUp slower wow" data-wow-delay="0.3s" "="" style="visibility: visible; animation-name: slideInUp; animation-delay: 0.3s;">
         <div class="card default-color-dark ">
            <div class="view">
               <?php the_post_thumbnail( 'medium-large', array( 'class'=> 'card-img-top')); ?>
               <a href="<?php the_permalink();?>" target="_blank">
               <div class="mask rgba-white-slight waves-effect waves-light"></div>
               </a>
            </div>
            <div class="card-body text-center">
               <b class="card-title white-text"><?php the_title(); ?></b><br>
               <a href="<?php the_permalink();?>" target="_blank" class="btn btn-outline-white btn-md waves-effect">Read More</a>
            </div>
         </div>
         </div>
        <?php
    }   
    }else 
    {
        echo 'Tidak Ada Post';    
    }
    ?>
  </div>
</main>
</div>
<div class="col-md-3">
<aside>
    <?php dynamic_sidebar('sidebar1');?>
    <?php dynamic_sidebar('sidebar2');?>
</aside>
</div>
</div>
</div>
<?php mdb_pagination(); ?>
</div>
<div class="clear"></div>
<?php get_footer();?>
?>