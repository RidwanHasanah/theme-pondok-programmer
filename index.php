<?php get_header(); ?>

<div class="produk p-5">
<div class="container-fluid pt-5 text-center text-white">
<h1 class="text-left">Halaman Blog</h1>
<div class="row">

<div class="col-md-9">
<main>
  <div class="row p-2">
    <?php
    if( have_posts())
    {
    while(have_posts())
    {
        the_post();
        get_template_part('content',get_post_format());
    }   
    }else 
    {
        echo 'Tidak Ada Post';    
    }
    ?>
  </div>
</main>
</div>
<div class="col-md-3">
<aside>
    <?php dynamic_sidebar('sidebar1');?>
    <?php dynamic_sidebar('sidebar2');?>
</aside>
</div>
</div>
</div>
<?php mdb_pagination(); ?>
</div>
<div class="clear"></div>
<?php get_footer();?>