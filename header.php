<!DOCTYPE html>
<!-- Displays the language attributes for the <html> tag https://codex.wordpress.org/Function_Reference/language_attributes  -->

<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="google-site-verification" content="Qi0wnNXGrGsYX96da3pHG259RJd9UUKRVQhV-8867RY" />
  <title><?php bloginfo('name'); ?></title>
  <?php wp_head();?>
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/img/brands/bazpusat.png';?>">
</head>
<style>
		@font-face {
			font-family: "Raleway Light";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_Light.ttf';?>);
		}

		@font-face {
			font-family: "Raleway Thin";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_Thin.ttf';?>);
		}

		@font-face {
			font-family: "Raleway Regular";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_Regular.ttf';?>);
		}

		@font-face {
			font-family: "Raleway Medium";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_Medium.ttf';?>);
		}

		@font-face {
			font-family: "Raleway Bold";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_Bold.ttf';?>);
		}

		@font-face {
			font-family: "Raleway SemiBold";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_SemiBold.ttf';?>);
		}

		@font-face {
			font-family: "Raleway ExtraBold";
			src: url(<?php echo get_template_directory_uri() . '/font/raleway/Raleway_ExtraBold.ttf';?>);
		}

		@font-face {
			font-family: "Quicksand Bold";
			src: url(<?php echo get_template_directory_uri() . '/font/qiucksand/Quicksand_Bold.otf';?>);
		}
		.landing {
			background-image: url(<?php echo get_template_directory_uri() . '/img/bg/bg-landing.svg';?>);
			background-attachment: fixed;
			background-size: cover;
			background-repeat: no-repeat;
			width: 100%;
			height: 100%;
			color: #FFFFFF;
			position: relative;
		}
		.hero-more {
			background-image: url(<?php echo get_template_directory_uri() . '/img/svg/bg-hero-more.svg';?>);
			background-size: contain;
			background-repeat: no-repeat;
			background-position: bottom;
		}
		.ceo {
			margin-top: -2px;
			background-image: url(<?php echo get_template_directory_uri() . '/img/svg/bg-hero-more-y.svg';?>);
			background-size: contain;
			background-repeat: no-repeat;
			background-position: top;
		}
		.testimoni {
			background-image: url(<?php echo get_template_directory_uri() . '/img/svg/bg-testimoni.svg';?>);
			background-size: contain;
			background-repeat: no-repeat;
			background-position: bottom;
		}

</style>
<body>
<?php require_once('components/front-page/navbar.inc.php'); ?>